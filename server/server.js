const app = require('./src/app');
const PORT = process.env.PORT || 3000;
const mongoose = require('mongoose');
const configDB = require('./src/config/database');
// Connect database 
mongoose
    .connect(configDB.DB_URL, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log('Connect database successfull');
    })
app.listen(PORT, () => console.log(`Server started on http://localhost:${PORT}`));