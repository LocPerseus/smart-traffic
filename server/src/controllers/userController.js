const bcrypt = require('bcryptjs');
const User = require('../model/User');


hashPassword = async(password) => {
    return await bcrypt.hash(password, 10);
}
validatePassword = async(hashedPassword, plainPassword) => {
    return await bcrypt.compare(plainPassword, hashedPassword);
}
exports.register = async(req, res) => {
    try {
        const { username, email, password } = req.body;
        const hashedPassword = await hashPassword(password);
        console.log(hashedPassword);
        const newUser = new User({
            username: username,
            email: email,
            password: hashedPassword
        });
        await newUser.save();
        res.status(201).json({
            status: 'success',
            message: 'Create user successfully'
        })
    } catch (error) {
        res.status(400).json({
            status: 'fail',
            result: 'Email or Username already exists',
            message: error
        })
    }
}
exports.login = async(req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(404).json({
                message: 'Email not exists'
            })
        }
        const validPass = await validatePassword(user.password, password);
        console.log(validPass);
        if (!validPass) {
            return res.status(404).json({
                message: 'Password not correct'
            })
        }
        res.status(200).json({
            status: 'success',
            message: 'Login successfully'
        })
    } catch (error) {
        res.status(400).json({
            status: 'fail',
            message: error
        })
    }
}