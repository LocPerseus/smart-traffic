const mongoose = require('mongoose');
const userSchema = mongoose.Schema({
    username: {
        type: String,
        require: [true, 'A user must have a username'],
        unique: true
    },
    email: {
        type: String,
        require: [true, 'A user must have a email'],
        unique: true
    },
    password: {
        type: String,
        require: [true, 'A user must have a password']
    }
})
const User = module.exports = mongoose.model('User', userSchema);